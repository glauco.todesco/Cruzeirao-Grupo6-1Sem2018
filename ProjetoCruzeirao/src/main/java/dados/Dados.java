package dados;

import java.util.*;
import entidades.*;

public class Dados {
	public static ArrayList<Campeonato> campeonatos = new ArrayList<Campeonato>();
	public static ArrayList<Cartao> cartoes = new ArrayList<Cartao>();
	public static ArrayList<Categoria> categorias = new ArrayList<Categoria>();
	public static ArrayList<Equipe> equipes = new ArrayList<Equipe>();
	public static ArrayList<Fase> fases = new ArrayList<Fase>();
	public static ArrayList<Gol> gols = new ArrayList<Gol>();
	public static ArrayList<Grupo> grupos = new ArrayList<Grupo>();
	public static ArrayList<Inscricao> inscricoes = new ArrayList<Inscricao>();
	public static ArrayList<Inscrito> inscritos = new ArrayList<Inscrito>();
	public static ArrayList<Juiz> juizes = new ArrayList<Juiz>();
	public static ArrayList<Local> locais = new ArrayList<Local>();
	public static ArrayList<Partida> partidas = new ArrayList<Partida>();
	public static ArrayList<PartidasFutebol> partidasfutebol = new ArrayList<PartidasFutebol>();
	public static ArrayList<Rodada> rodadas = new ArrayList<Rodada>();
	public static ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
	
	// ArrayList's auxiliares de usu�rio
	
	public static ArrayList<Usuario> usuarioJuizes = new ArrayList<Usuario>();
	public static ArrayList<Usuario> usuarioDiretores = new ArrayList<Usuario>();
	public static ArrayList<Usuario> usuarioComissao = new ArrayList<Usuario>();
	public static ArrayList<Usuario> usuarioJogadores = new ArrayList<Usuario>();
	
}
